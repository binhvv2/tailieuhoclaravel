<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php  
        $i=1;
        $pass = $email =  $err_email = $err_pass = $err_file =  $file_name = "";
        $array = array("image/png","image/jpg","image/jpeg","image/JPG", "image/PNG");
        if(isset($_POST["submit"])){
            $email = $_POST["email"];
            $pass = $_POST["pass"];
            $file = $_FILES["avatar"];
            $file_name = $_FILES["avatar"]["name"];
            if($email == null){
                $err_email = "Vui Lòng Nhập Email";
                $i=2;
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $err_email = "Vui Lòng Nhập Đúng Định Dạng Email";
                $i=2;
            }
            if($pass == null){
                $err_pass = "Vui Lòng Nhập Pass";
                $i=2;
            }
            else if(strlen($pass)>12 || strlen($pass)<6){
                $err_pass = "Vui Lòng Nhập từ 6 đến 12 ký tự";
                $i=2;
            }
            if(!empty($file)){
                if($_FILES["avatar"]["size"]>1048576){
                    $err_file = "Vui Lòng Chọn Ảnh Dưới 1MB";
                    $i=2;
                }
                if(!in_array($_FILES["avatar"]["type"],$array)){
                    $err_file = "Vui Lòng chọn định dạng ảnh jpg,png, hoặc jpeg";
                    $i=2;
                }
            }
            else{
                $err_file = "Vui Lòng chọn ảnh";
                $i=2;
            }
            if($i==1){
                move_uploaded_file($_FILES["avatar"]["tmp_name"],"./img/".$_FILES["avatar"]["name"]);
                $_SESSION["email"] = $email;
                $_SESSION["pass"] = $pass;
                header("location:login.php");
            }
         
        }
    
    ?>
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
    <p>Email:</p>
        <input type="text" name="email" value="<?php echo $email; ?>">
        <p><?php echo $err_email; ?></p>
    <p>Pass:</p>
        <input type="pass" name="pass" value="<?php echo $pass; ?>">
        <p><?php echo $err_pass; ?></p>
    <p>Avatar:</p>
        <input type="file" name="avatar" value="<?php echo $file_name; ?>">
        <p><?php echo $err_file; ?></p>
        <input type="submit" name="submit" value="Đăng Ký">
    </form>
</body>
</html>