<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php
        $email = $pass = $err_email = $err_pass = $err ="";
        $i = true;
        if(isset($_SESSION)){
            $email_ss = $_SESSION["email"];
            $pass_ss = $_SESSION["pass"];
        
            if(isset($_POST["submit"])){
                $email = $_POST["email"];
                $pass = $_POST["pass"];
                if(empty($email)){
                    $err_email = "Nhập Email";
                    $i=false;
                }
                else if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                    $err_email = "Nhập Đúng Định Dạng Email ";
                    $i=false;
                }
                if(empty($pass)){
                    $err_pass = "Nhập Pass";
                    $i=false;
                }
                else if(strlen($pass)>12 || strlen($pass)<6){
                    $err_pass = "Nhập Pass từ 6 đến 12 ký tự";
                    $i=false;
                }

                if($i==true){
                    if($email != $email_ss || $pass != $pass_ss){
                            $err = "Email Hoặc Pass Chưa Đúng";
                    }
                    else{
                        $err = "Đăng Nhập Thành Công";
                            // header("location:https://google.com");
                    }
                }
            }
        }
        else{
            header("location:register.php");
        }
    ?>
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        <p>Email:</p>
            <input type="text" name="email" value="<?php echo $email; ?>">
            <p><?php echo $err_email; ?></p>
        <p>Pass:</p>
            <input type="pass" name="pass" value="<?php echo $pass; ?>">
            <p><?php echo $err_pass; ?></p>
            <input type="submit" name="submit" value="Đăng nhập">
            <p><?php echo $err; ?></p>
    </form>
</body>
</html>