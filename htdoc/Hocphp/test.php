<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            .erro{
                color: red;
            }
        </style>
    </head>
    <body>

        <?php
            $a = 0;
            $erro_mail = "";
            $erro_pass = "";
            $erro_avt = "";

            if (isset($_POST['register'])) {
                if (empty($_POST['email'])) {
                    $erro_mail = 'Vui lòng nhập email';
                    $a = 1;
                }
                if (empty($_POST['pass'])) {
                    $erro_pass = 'Vui lòng nhập pass';
                    $a = 1;
                }
                if (empty($_FILES['avatar']['name'])) {
                    $erro_avt = 'Vui lòng tải lên avatar';
                    $a = 1;
                }
                else {
                    if ($_FILES['avatar']['size'] > (1024 * 1024)) {
                        $erro_avt= 'Ảnh avatar > 1Mb';
                        // var_dump($_FILES['avatar']['size']);
                        $a = 1;
                        }
                    if ($_FILES['avatar']['name']) {
                        $image = array("png", "jpg", "jpeg", "PNG", "JPG");
                        $duoi = ($_FILES['avatar']['name']);
                        echo "<pre>";
                        // var_dump($duoi);
                        $x = explode('.', $duoi);
                        // var_dump($x);
                        if (in_array($x[1], $image) == false) {
                            echo 'Đây không phải là file hình ảnh';
                            $a = 1;
                        }
                    }
                }
                if ($a == 0) {
                    move_uploaded_file($_FILES['avatar']['tmp_name'], 'E:\Xamp\htdocs\php\upload'.$_FILES['avatar']['name']);
                    
                    echo 'Đăng ký thành công';
                }
            }
        ?>

        <form method="post" action="" enctype="multipart/form-data"> 
            <p>Email :</p>
            <input type="text" name="email" value="" />
            <p class="erro"><?php echo $erro_mail; ?></p>
            <p>Mật khẩu :</p>
            <input type="text" name="pass" value="" />
            <p class="erro"><?php echo $erro_pass; ?></p>
            <p>Hình đại diện :</p>
            <input type="file" name="avatar" value="" /></br></br>
            <p class="erro"><?php echo $erro_avt; ?></p>
            <input type="submit" name="register" value="Đăng ký" />
        </form>

    </body>
</html>