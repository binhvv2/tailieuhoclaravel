<?php
//- tao table :id, name, email, avatar
CREATE TABLE nguoidung(
	id int(12) NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    avatar varchar(255),
    CONSTRAINT pk_id PRIMARY KEY(id)
)ENGINE=INNODB

//- viet cau lenh sql insert 4 data vào
INSERT into nguoidung(name,email,avatar) VALUES('Binh','binhvv20051996@gmail.com','avatar1.jpg');
INSERT into nguoidung(name,email,avatar) VALUES('Tam','tam1996@gmail.com','avatar2.jpg');
INSERT into nguoidung(name,email,avatar) VALUES('Hung','Hung1996@gmail.com','avatar3.jpg');
INSERT into nguoidung(name,email,avatar) VALUES('My','My1998@gmail.com','avatar4.jpg');

// viet cau lenh sql lay all data ra
select*from nguoidung

// viet cau lenh sql lay 1 data theo ID bat ky
select*from nguoidung where id=1;

//viet cau len sql lay 1 data theo ID va email (where id =id and email = email)
select*from nguoidung where id=0 and email = 'binhvv20051996@gmail.com';

//viet cau lenh update data theo ID
update nguoidung set name='Võ Văn Bình',email = 'binh@gmail.com' where id = 0;

// viet cau lenh xoa 1 data theo ID
delete from nguoidung where id = 0;
?>
